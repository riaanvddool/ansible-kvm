Ansible KVM Host Playbook
=========================

An Ansible Playbook for creating and controlling KVM virtual machines on a remote host 
--------------------------------------------------------------------------------------


Based on a combination of Artem Vasilyev's <a href="https://medium.com/@artem.v.vasilyev/use-ubuntu-cloud-image-with-kvm-1f28c19f82f8
" target="_blank">Use Ubuntu Cloud Image with KVM</a> article and Stephen Fromm's <a href="https://github.com/sfromm/ansible-playbooks/blob/master/virt-guests.yml" target="_blank">playbook in Github</a>.

This playbook uses Ubuntu Cloud images to avoid the manual install process required in the playbook linked to above.


Usage:
------

1. Place your SSH public key in `group_vars/vmhosts.yaml`:

      
	```vm_ssh_pubkey: "ssh-rsa XXXXXXXXXXX keyname"```


2. Set up your hosts in `inventory` and add them to the vmhosts group


3. Set up your vm guests in `roles/vmhost/vars/main.yaml`


4. Execute the playbook to create and run the remote VMs:

     
	```ansible-playbook -i inventory site.yaml --ask-become-pass```


5. Add `--tags removeguests` to remove the VMs again:

      
	```ansible-playbook -i inventory site.yaml --ask-become-pass --tags removeguests```


Acknowledgements:
-----------------

A special thanks to <a href="https://github.com/cynici">Cheewai</a> for helping me with debugging.  

Artem Vasilyev's <a href="https://medium.com/@artem.v.vasilyev/use-ubuntu-cloud-image-with-kvm-1f28c19f82f8
" target="_blank">Use Ubuntu Cloud Image with KVM</a>

Stephen Fromm's <a href="https://github.com/sfromm/ansible-playbooks/blob/master/virt-guests.yml" target="_blank">playbook in Github</a>.
